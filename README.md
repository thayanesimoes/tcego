# TCEGO


## Run project
```
--datadir
    Path to the folder where the image dataset is stored
--usecase
    Use case name, default buracos
--folderdate
    Name of the folder to save the train information
--shuffle 
    Should one shuffle data after considering temporal restriction
--chooseempty
    Should one keep only 10 percent of empty images

Script configurado para fazer merge entre as taks que possuem o prefixo "task"
```
python merge_cvat.py --datadir path_dir/datasets/ --usecase buracos --folderdate images --shuffle --chooseempty


