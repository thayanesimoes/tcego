import os
import shutil
from shutil import copyfile
import sys
import argparse
import cv2 as cv
import random
import numpy as np
parser = argparse.ArgumentParser(description='Merge multiple yolo tasks from Cvat')
parser.add_argument('--datadir', help='Data directory.', default='', type=str)
parser.add_argument('--unzip', help='Unzip folders', action = 'store_true')
parser.add_argument('--usecase', help='Usecase. Options are: buracos', default='buracos', type=str)
parser.add_argument('--folderdate', help='Name of the folder to save the train information', default='temp', type=str)
parser.add_argument('--checkbboxes',help='Should one create folders for each class containing all bboxes from that specific class',action = 'store_true')
parser.add_argument('--discard_empty_frames',help='Should one discard empty frames',action = 'store_true')
parser.add_argument('--shuffle',help='Should one shuffle data after considering temporal restriction',action = 'store_true')
parser.add_argument('--chooseempty',help='Should one keep only 10 percent of empty images',action = 'store_true')
parser.add_argument('--considerroi',help='', action = 'store_true')

args = parser.parse_args()

datadir = args.datadir
folderdate = args.folderdate

if os.path.exists(os.path.join(datadir,folderdate)):
    shutil.rmtree(os.path.join(datadir,folderdate))
os.makedirs(os.path.join(datadir,folderdate))
os.makedirs(os.path.join(datadir,folderdate,'obj_data'))
tasks = os.listdir(datadir)
if args.unzip:
    for task in tasks:
        if '.zip' in task:
            foldername = datadir + task.split('.zip')[0]
            if os.path.exists(foldername):
                shutil.rmtree(foldername)
            command = 'unzip ' + os.path.join(datadir, task) + ' -d ' + foldername
            print(command)
            os.system(command)
def standardize_category(old_category,usecase):
    new_category = None
    new_category_id = None
    if usecase == 'buracos':
        if old_category == 'buraco' or old_category == 'buracos':
            new_category = 'buracos'
        if new_category is not None:
            new_category_id = standard_category_id[new_category]
    
    return new_category_id, new_category
def cv2yolo(l,r,t,b, dw_cropped, dh_cropped):
    w1 = r - l
    h1 = b - t
    x1 = l + w1/2
    y1 = t + h1/2
    norm_w1 = w1 / dw_cropped
    norm_h1 = h1 / dh_cropped
    norm_x1 = x1 / dw_cropped
    norm_y1 = y1 / dh_cropped
    return (norm_x1,norm_y1,norm_w1,norm_h1)
def yolo2cv2(x,y,w,h,dw,dh):
    #CONVERT x,y,w,h to cv2 bbox
    l = int((x - w / 2) * dw)
    r = int((x + w / 2) * dw)
    t = int((y - h / 2) * dh)
    b = int((y + h / 2) * dh)
    if l < 0:
        l = 0
    if r > dw - 1:
        r = dw - 1
    if t < 0:
        t = 0
    if b > dh - 1:
        b = dh - 1
    return(l,r,t,b)
def toYoloAnn(x1, y1, x2, y2, w, h):
    xDark = (x2 - (x2 - x1)/2) / w
    yDark = (y2 - (y2 - y1)/2) / h
    wDark = (x2 - x1) / w
    hDark = (y2 - y1) / h
    return xDark, yDark, wDark, hDark
if args.usecase == 'buracos':
    standard_category_id = {'buracos': 0}
    categories_count = {'buracos': 0}
    categories_of_interest = ['buracos']
    roi = [15,970,290,1710] # [y1,y2,x1,x2]
if args.checkbboxes:
    if os.path.exists(os.path.join(datadir,folderdate,'checkbboxes')):
        shutil.rmtree(os.path.join(datadir,folderdate,'checkbboxes'))
    os.makedirs(os.path.join(datadir,folderdate,'checkbboxes'))
    for category in categories_of_interest:
        os.makedirs(os.path.join(datadir,folderdate,'checkbboxes',category))
imgtrain = 0
imgval = 0
imgtest = 0
count = 0
for task in tasks:
    files = []
    if 'task' in task and 'zip' not in task:
        annimgsontask = 0
        print('Processing task: ' + task)
        my_file = open(datadir + task + '/obj.names', "r")
        obj_names = my_file.read()
        task_category_id = {}
        count_names = 0
        for name in obj_names.split('\n'):
            if not name == '':
                task_category_id[name] = count_names
                count_names += 1
        my_file = open(datadir + task + '/train.txt', "r")
        df = my_file.read()
        df = df.split(".jpg")
        my_file.close()
        n_images = len(df)
        n_empty = 0
        for ann_path in df:
            if not ann_path == '\n': 
                try:
                    frame_name = ann_path.split("/")[2]
                except IndexError:
                    print(ann_path)
                with open(os.path.join(datadir,task,'obj_train_data',frame_name+'.txt'), "r") as f:
                    ann = f.read()
                selected_anns = []
                old = os.path.join(datadir,task,'obj_train_data', frame_name )
                new = os.path.join(datadir,folderdate,'obj_data','frame_'+ str(count))
                if not ann == "":
                    ann_count = 0
                    count +=1
                    if args.considerroi:
                        img = cv.imread(old+".jpg")   
                        img_cropped = img[roi[0]:roi[1], roi[2]:roi[3]]
                        dh,dw,_ = img.shape
                        dh_cropped,dw_cropped,_ = img_cropped.shape
                        cv.imwrite(new+".jpg", img_cropped)
                    else:
                        shutil.copy(old+".jpg", new+".jpg")
                    files.append('frame_' + str(count) + '.jpg')
                    splitted_ann = ann.split('\n')
                    higher_y = None
                    ycenters = []
                    for an in splitted_ann:
                        if not an == '':
                            an_list = list(an.split(' '))
                            ycenters.append(float(an_list[2]))
                    higher_y = np.argmax(ycenters)
                    for n, an in enumerate(splitted_ann):
                        if not an == '':
                            old_category = list(task_category_id.keys())[list(task_category_id.values()).index(int(an[0]))]
                            standardized_category_id, standardized_category = standardize_category(old_category,args.usecase)
                            an_list = list(an.split(' '))
                            xcenter = float(an_list[1])
                            ycenter = float(an_list[2])
                            # if 0.45 < xcenter < 0.6:
                            #     if n == higher_y:
                            #         standardized_category_id = 1
                            #         standardized_category = 'default'
                            if standardized_category in categories_of_interest:
                                standardized_an = str(standardized_category_id) + ' ' + ' '.join(an.split()[1:])
                                selected_anns.append(standardized_an)
                                if args.checkbboxes:
                                    orig_frame = cv.imread(os.path.join(datadir,task,'obj_train_data',frame_name+'.jpg'))
                                    h,w,_ = orig_frame.shape
                                    an = list(an.split(' '))
                                    xcenter = float(an[1])*w
                                    ycenter = float(an[2])*h
                                    bboxw = float(an[3])*w
                                    bboxh = float(an[4])*h
                                    l = int((xcenter - bboxw / 2))
                                    r = int((xcenter + bboxw / 2))
                                    t = int((ycenter - bboxh / 2))
                                    b = int((ycenter + bboxh / 2))
                                    bbox_img = orig_frame[t:b, l:r]
                                    cv.imwrite(os.path.join(datadir,folderdate,'checkbboxes',standardized_category,task+frame_name+'_'+str(ann_count)+'.png'),bbox_img)
                                    ann_count += 1
                    if not selected_anns == []:
                        selected_anns = set(selected_anns)
                        with open(new + '.txt', 'w') as f:
                            for item in selected_anns:
                                category = list(standard_category_id.keys())[list(standard_category_id.values()).index(int(item.split(' ')[0]))]
                                categories_count[category] = categories_count[category] + 1
                                if args.considerroi:
                                    label, x, y, w, h = item.split(' ')
                                    x = float(x) - roi[2]/dw
                                    y = float(y) - roi[0]/dh
                                    l,r,t,b = yolo2cv2(x, y, float(w), float(h), dw, dh)
                                    x1,y1,w1,h1 = cv2yolo(l,r,t,b, dw_cropped, dh_cropped)
                                    add_line = str(label)+" "+str(x1)+" "+str(y1)+" "+str(w1)+" "+str(h1)+ "\n"
                                    f.write(add_line)
                                else:
                                    f.write("%s\n" % item)                        
                        annimgsontask += 1
                elif ann == "" and not args.discard_empty_frames:
                    if args.chooseempty:
                        if n_empty <= n_images*0.1:
                            if args.considerroi:
                                img = cv.imread(old+".jpg")   
                                img_cropped = img[roi[0]:roi[1], roi[2]:roi[3]]
                                dh,dw,_ = img.shape
                                dh_cropped,dw_cropped,_ = img_cropped.shape
                                cv.imwrite(new+".jpg", img_cropped)
                            else:
                                shutil.copy(old+".jpg", new+".jpg")
                            files.append('frame_' + str(count) + '.jpg')
                            shutil.copy(old+".txt", new+".txt")
                            count +=1
                            n_empty += 1
                    else:
                        if args.considerroi:
                            img = cv.imread(old+".jpg")   
                            img_cropped = img[roi[0]:roi[1], roi[2]:roi[3]]
                            dh,dw,_ = img.shape
                            dh_cropped,dw_cropped,_ = img_cropped.shape
                            cv.imwrite(new+".jpg", img_cropped)
                        else:
                            shutil.copy(old+".jpg", new+".jpg")
                        files.append('frame_' + str(count) + '.jpg')
                        shutil.copy(old+".txt", new+".txt")
                        count +=1
        print(categories_count)
        print('imgs in task ' + str(annimgsontask))
        train = files[:int(len(files)*0.7)]
        val = files[int(len(files)*0.7):int(len(files)*0.9)]
        test = files[int(len(files)*0.9):]
        if args.shuffle:
            random.shuffle(train)
            random.shuffle(val)
            random.shuffle(test)
        imgtrain += len(train)
        imgval += len(val)
        imgtest += len(test)
        with open(os.path.join(datadir,folderdate,'shuffle_train.txt'), 'a') as f:
            for item in train:
                file = os.path.join(datadir,folderdate,'obj_data',item)
                f.write("%s\n" % file)
        with open(os.path.join(datadir,folderdate,'shuffle_val.txt'), 'a') as f:
            for item in val:
                file = os.path.join(datadir,folderdate,'obj_data',item)
                f.write("%s\n" % file)
        with open(os.path.join(datadir,folderdate,'shuffle_test.txt'), 'a') as f:
            for item in test:
                file = os.path.join(datadir,folderdate,'obj_data',item)  
                f.write("%s\n" % file)
with open(os.path.join(datadir,folderdate,'obj.names'), 'a') as f:
    for item in standard_category_id.keys():
        f.write("%s\n" % item)
print('Categories count')
print(categories_count)
print("Images on train set: " + str(imgtrain))
print("Images on val set: " + str(imgval))
print("Images on test set: " + str(imgtest))